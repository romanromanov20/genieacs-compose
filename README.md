# genieacs-compose
GenieACS on Docker Compose

Make sure you have Docker and Docker-Compose on your server.
To start GenieACS server just download docker-compose.yml and run 

> docker-compose up

To build your own docker image locally use Dockerfile.

Username and password is admin/admin, these can be changed by modifying users.yml file.

In OLT provide default username and password for ONT authentification, username: huawei and password MyGeniePassword

Do not forget to restrict access to web and API with iptables filter on host machine.

